# GoCount

GoCount is a simple timer written in Go. It allows you to set a countdown timer and notifies you when the time is up.

![Example GIF](./example.gif)

## Features
- [x] Ability to pipe into curl to send to ntfy instance (`&& curl -k -d "Timer triggered" https://ntfy.example.lan/TOPIC`)
- [x] Ability to countdown from a specified duration

## Installation

To use the timer, you need to have Go installed on your machine. You can download and install Go from the official website: [https://golang.org/dl/](https://golang.org/dl/)

Once you have Go installed, you can clone this repository using the following command:

`git clone https://gitlab.com/dexter8242/gocount.git`

## Usage

To use the timer, navigate to the cloned repository's directory and build the executable using the following command:

`go build`


After successfully building the executable, you can run the timer using the following command:

`./count <duration> <optional log level, defaults to info>`

I.e:

`./count 5m warn`

The timer will prompt you to enter the duration in seconds. Once you enter the duration, the timer will start counting down. When the time is up, the timer will notify you.

## Contributing

Contributions are welcome! If you find any issues or have any suggestions for improvements, feel free to open an issue or submit a pull request.

## License

This project is licensed under the MIT License.


Feel free to customize this README.md according to your specific timer implementation and requirements.
