package main

import (
	"fmt"
	"github.com/charmbracelet/log"
	"os"
	"time"
)

func main() {
	log.Debug("Program is starting")
	// Get number of flags provided
	numArgs := len(os.Args) - 1
	if numArgs < 1 {
		log.Error("Not enough arguments provided to program, expected at least 1")
		log.Fatal("Usage: ./count <duration, i.e 5m> <optional log level (defaults to info)>")
	}

	// Set duration to first argument
	durationStr := os.Args[1]

	duration, err := time.ParseDuration(durationStr)
	if err != nil {
		log.Fatal("Invalid duration format", "err", err)
	}

	// Variable for log level (defaults to info)
	var logLevel string

	if len(os.Args)-1 == 1 {
		logLevel = "info"
	} else {
		logLevel = os.Args[2]
	}

	// Initialize the logger with the user specified log-level (defaults to info)
	logger := initLogger(logLevel)
	logger.Info("Initialized logger")

	// Make a channel for when the timer fires
	done := make(chan bool)

	logger.Info("Starting timer")

	err = startTimer(duration, done, logger)
	if err != nil {
		logger.Fatal("An error occured when starting the timer", "err", err)
	}
}

func initLogger(logLevel string) *log.Logger {

	// Create a new logger to Stderr
	logger := log.New(os.Stderr)

	// Match the loglevel and set it
	switch logLevel {
	case "debug":
		logger.SetLevel(log.DebugLevel)
	case "info":
		logger.SetLevel(log.InfoLevel)
	case "warn":
		logger.SetLevel(log.WarnLevel)
	case "error":
		logger.SetLevel(log.ErrorLevel)
	case "fatal":
		logger.SetLevel(log.FatalLevel)
	}

	return logger
}

func startTimer(duration time.Duration, done chan bool, logger *log.Logger) error {
	// Ticks each second in order to show remaining time
	ticker := time.NewTicker(1 * time.Second)
	logger.Debug("startTimer(): created ticker")

	// Stop the ticker once everything is done
	defer ticker.Stop()

	go func() {
		for remainingTime := duration; remainingTime >= 0; remainingTime -= time.Second {
			select {
			// Show remaining time each second
			case <-ticker.C:
				logger.Debug("startTimer(): Sending tick...")
				fmt.Printf("\rTime remaining: %v", remainingTime)
			case <-done:
				return
			}
		}
		sendNotification(done, logger)
	}()

	<-done

	return nil
}

func sendNotification(done chan bool, logger *log.Logger) {
	fmt.Println()
	logger.Warn("Timer fired!")
	defer os.Exit(0)

	done <- true
}
